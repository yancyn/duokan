package com.muje.lockscreen;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.io.File;
import java.io.IOException;

public class FirstFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final WallpaperManager wallpaperManager = WallpaperManager.getInstance(this.getContext());

        TextView textView = view.findViewById(R.id.textView);
        textView.setText("Support wallpaper: " + wallpaperManager.isWallpaperSupported());

        TextView textView2 = view.findViewById(R.id.textView2);
        textView2.setText("Allow to set wallpaper: " + wallpaperManager.isSetWallpaperAllowed());

        // display what is system wallpaper
        //ImageView imageView = view.findViewById(R.id.imageView);
        //imageView.setImageDrawable(wallpaperManager.getDrawable());

        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                NavHostFragment.findNavController(FirstFragment.this).navigate(R.id.action_FirstFragment_to_SecondFragment);

                // change wallpaper
                //Bitmap bitmap = BitmapFactory.decodeFile(path);
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.july);
                BitmapDrawable drawable = new BitmapDrawable(bitmap);
                try {
                    wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_SYSTEM); // FLAG_LOCK
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
